<?php
/**
* 2007-2021 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2021 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

if (file_exists(_MODULE_DIR_ . 'managebotabs/vendor/autoload.php')) {
    require_once(_MODULE_DIR_ . 'managebotabs/vendor/autoload.php');
}

class Managebotabs extends Module
{
    protected $config_form = true;

    public function __construct()
    {
        $this->name = 'managebotabs';
        $this->tab = 'administration';
        $this->version = '1.1.1';
        $this->author = 'Ciren';
        $this->need_instance = 1;
        $this->module_key = 'cb86dbce0ceef210cbd2fdcedb2096e0';

        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->trans('Manage admin menus', [], 'Modules.Managebotabs.Admin');
        $this->description = $this->trans('Manage admin menus', [], 'Modules.Managebotabs.Admin');

        $this->ps_versions_compliancy = array('min' => '1.7.6.0', 'max' => '1.7.8.99');
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        $this->addTranslations();

        $this->tabs = [
            [
                'class_name' => 'AdminManageBoTabs',
                'visible' => true,
                'name' => $this->trans('Menus BO', [], 'Modules.Managebotabs.Admin'),
                'parent_class_name' => 'AdminParentThemes',
            ],
        ];

        if (!parent::install()) {
            $this->deleteTranslations();
            return false;
        }

        return $this->addTab();
    }

    protected function addTab()
    {
        $tabRepository = $this->get('prestashop.core.admin.tab.repository');

        if (!$tabRepository->findOneIdByClassName('AdminManageBoTabs')) {
            $tab = new Tab();
            $tab->class_name = 'AdminManageBoTabs';
            $tab->id_parent = (int)$tabRepository->findOneIdByClassName('AdminParentThemes');
            $tab->module = $this->name;
            $tab->name[(int) Configuration::get('PS_LANG_DEFAULT')] = $this->trans(
                'Menus BO',
                [],
                'Modules.Managebotabs.Admin'
            );
            $tab->add();
        }

        return true;
    }

    protected function addTranslations()
    {
        $translations = $this->getTranslations();

        foreach (Language::getLanguages() as $language) {
            if (array_key_exists($language['iso_code'], $translations)) {
                foreach ($translations[$language['iso_code']] as $translation) {
                    $translation['id_lang'] = $language['id_lang'];

                    Db::getInstance()->insert('translation', $translation);
                }
            }
        }

        return true;
    }

    protected function getTranslations()
    {
        return array(
            'fr' => array(
                array(
                    'key' => 'Manage admin menus',
                    'translation' => 'Gérer les menus du back-office',
                    'domain' => 'ModulesManagebotabsAdmin'
                ),
                array(
                    'key' => 'Menus BO',
                    'translation' => 'Menus BO',
                    'domain' => 'ModulesManagebotabsAdmin'
                ),
                array(
                    'key' => 'Edit the tab',
                    'translation' => 'Modifier le menu',
                    'domain' => 'ModulesManagebotabsAdmin'
                ),
                array(
                    'key' => 'Class',
                    'translation' => 'Classe',
                    'domain' => 'ModulesManagebotabsAdmin'
                ),
                array(
                    'key' => 'Display selection',
                    'translation' => 'Afficher la sélection',
                    'domain' => 'ModulesManagebotabsAdmin'
                ),
                array(
                    'key' => 'Do not display selection',
                    'translation' => 'Ne pas afficher la sélection',
                    'domain' => 'ModulesManagebotabsAdmin'
                ),
            )
        );
    }

    public function uninstall()
    {
        if (!parent::uninstall()) {
            return false;
        }

        return $this->deleteTranslations() && $this->removeTab();
    }

    protected function removeTab()
    {
        $tabRepository = $this->get('prestashop.core.admin.tab.repository');

        if ($tabId = (int)$tabRepository->findOneIdByClassName('AdminManageBoTabs')) {
            $tab = new Tab($tabId);
            $tab->delete();
        }

        return true;
    }

    protected function deleteTranslations()
    {
        Db::getInstance()->delete('translation', 'domain = "ModulesManagebotabsAdmin"');

        return true;
    }

    public function getContent()
    {
        $link = $this->context->link->getAdminLink('AdminManageBoTabs');
        Tools::redirectAdmin($link);
    }

    public function isUsingNewTranslationSystem()
    {
        return true;
    }
}
