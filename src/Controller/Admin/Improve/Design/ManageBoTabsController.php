<?php
/**
 * 2007-2021 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA
 * @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

namespace PrestaShop\Module\ManageBoTabs\Controller\Admin\Improve\Design;

use PrestaShop\Module\ManageBoTabs\Core\Domain\Tab\Command\BulkDisableTabCommand;
use PrestaShop\Module\ManageBoTabs\Core\Domain\Tab\Command\BulkEnableTabCommand;
use PrestaShop\Module\ManageBoTabs\Core\Domain\Tab\Command\EnableTabCommand;
use PrestaShop\Module\ManageBoTabs\Core\Domain\Tab\Command\DisableTabCommand;
use PrestaShop\Module\ManageBoTabs\Core\Domain\Tab\Command\ToggleStatusTabCommand;
use PrestaShop\Module\ManageBoTabs\Core\Domain\Tab\Exception\TabException;
use PrestaShop\Module\ManageBoTabs\Core\Search\Filters\ManageBoTabsFilters;
use PrestaShop\Module\ManageBoTabs\Repository\ManageBoTabsRepository;
use PrestaShop\PrestaShop\Core\Domain\Customer\Exception\CustomerException;
use PrestaShop\PrestaShop\Core\Grid\GridFactory;
use PrestaShopBundle\Controller\Admin\FrameworkBundleAdminController;
use PrestaShopBundle\Security\Annotation\AdminSecurity;
use PrestaShopBundle\Security\Annotation\ModuleActivated;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ManageBoTabsController.
 *
 * @ModuleActivated(moduleName="managebotabs", redirectRoute="admin_module_manage")
 */
class ManageBoTabsController extends FrameworkBundleAdminController
{
    /**
     * @AdminSecurity("is_granted('read', request.get('_legacy_controller'))", message="Access denied.")
     *
     * @param int $parentId
     * @param Request $request
     * @param ManageBoTabsFilters $filters
     *
     * @return Response
     */
    public function listAction(Request $request, ManageBoTabsFilters $filters, $parentId = 0)
    {
        \Configuration::updateValue('MTB_ID_PARENT', (int)$parentId);
        /** @var GridFactory $factory */
        $factory = $this->get('prestashop.module.manage_bo_tabs.grid.factory');
        $grid = $factory->getGrid($filters);

        return $this->render(
            '@Modules/managebotabs/views/PrestaShop/Admin/Improve/Design/ManageBoTabs/list.html.twig',
            array(
                'grid' => $this->presentGrid($grid),
                'enableSidebar' => true,
                'help_link' => $this->generateSidebarLink($request->attributes->get('_legacy_controller'))
            )
        );
    }

    /**
     * @AdminSecurity("is_granted('update', request.get('_legacy_controller'))", message="Access denied.")
     *
     * @param Request $request
     * @param int $tabId
     *
     * @return Response
     */
    public function editAction(Request $request, $tabId)
    {
        $id_parent = (int)\Configuration::get('MTB_ID_PARENT');
        $tabFormBuilder = $this->get(
            'prestashop.module.manage_bo_tabs.core.form.identifiable_object.builder.form_builder'
        );
        $tabForm = $tabFormBuilder->getFormFor($tabId);

        $tabForm->handleRequest($request);

        $tabFormHandler = $this->get(
            'prestashop.module.manage_bo_tabs.core.form.identifiable_object.handler.form_handler'
        );
        // we use handleFor() instead of handle() since we now have an id
        $result = $tabFormHandler->handleFor($tabId, $tabForm);

        if ($result->isSubmitted() && $result->isValid()) {
            $this->addFlash('success', $this->trans('Successful update.', 'Admin.Notifications.Success'));

            return $this->redirectToRoute('admin_manage_bo_tabs_list', ['parentId' => (int)$id_parent]);
        }

        return $this->render(
            '@Modules/managebotabs/views/PrestaShop/Admin/Improve/Design/ManageBoTabs/edit.html.twig',
            [
                'mtbForm' => $tabForm->createView(),
                'tabId' => $tabId,
            ]
        );
    }

    /**
     * @AdminSecurity("is_granted('update', request.get('_legacy_controller'))", message="Access denied.")
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return RedirectResponse
     */
    public function updatePositionsAction(Request $request)
    {
        $id_parent = (int)\Configuration::get('MTB_ID_PARENT');
        $positionsData = [
            'positions' => $request->request->get('positions', null),
            'parentId' => $id_parent,
        ];

        /** @var ManageBoTabsRepository $repository */
        $repository = $this->get('prestashop.module.manage_bo_tabs.repository');

        try {
            $repository->updatePositions($id_parent, $positionsData);
            $this->addFlash('success', $this->trans('Successful update.', 'Admin.Notifications.Success'));
        } catch (DatabaseException $e) {
            $errors = [$e->getMessage()];
            $this->flashErrors($errors);
        }

        return $this->redirectToRoute('admin_manage_bo_tabs_list', ['parentId' => (int)$id_parent]);
    }

    /**
     * Toggle tab status.
     *
     * @AdminSecurity(
     *     "is_granted('update', request.get('_legacy_controller'))",
     *     redirectRoute="admin_customers_index",
     *     message="You do not have permission to edit this."
     * )
     *
     * @param int $tabId
     *
     * @return RedirectResponse
     */
    public function toggleStatusAction($tabId)
    {
        try {
            $command = new ToggleStatusTabCommand($tabId);

            $this->getCommandBus()->handle($command);

            $this->addFlash('success', $this->trans('Successful update.', 'Admin.Notifications.Success'));
        } catch (CustomerException $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute(
            'admin_manage_bo_tabs_list',
            ['parentId' => (int)\Configuration::get('MTB_ID_PARENT')]
        );
    }

    /**
     * Display Tab action.
     *
     * @AdminSecurity(
     *     "is_granted('update', request.get('_legacy_controller'))",
     *     redirectRoute="admin_manage_bo_tabs_list",
     *     message="You do not have permission to edit this."
     * )
     *
     * @param int $tabId
     *
     * @return RedirectResponse
     */
    public function enableAction($tabId)
    {
        try {
            $command = new EnableTabCommand($tabId);

            $this->getCommandBus()->handle($command);

            $this->addFlash('success', $this->trans('Successful update.', 'Admin.Notifications.Success'));
        } catch (CustomerException $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('admin_manage_bo_tabs_list');
    }

    /**
     * Hide Tab action.
     *
     * @AdminSecurity(
     *     "is_granted('update', request.get('_legacy_controller'))",
     *     redirectRoute="admin_manage_bo_tabs_list",
     *     message="You do not have permission to edit this."
     * )
     *
     * @param int $tabId
     *
     * @return RedirectResponse
     */
    public function disableAction($tabId)
    {
        try {
            $command = new DisableTabCommand($tabId);

            $this->getCommandBus()->handle($command);

            $this->addFlash('success', $this->trans('Successful update.', 'Admin.Notifications.Success'));
        } catch (CustomerException $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute('admin_manage_bo_tabs_list');
    }

    /**
     * Enable tabs in bulk action.
     *
     * @AdminSecurity(
     *     "is_granted('update', request.get('_legacy_controller'))",
     *     redirectRoute="admin_manage_bo_tabs_list",
     *     message="You do not have permission to edit this."
     * )
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function enableBulkAction(Request $request)
    {
        $tabIds = array_map(function ($tabId) {
            return (int) $tabId;
        }, $request->request->get('manage_bo_tabs_managebotabs_bulk', []));

        try {
            $command = new BulkEnableTabCommand($tabIds);

            $this->getCommandBus()->handle($command);

            $this->addFlash('success', $this->trans('Successful update.', 'Admin.Notifications.Success'));
        } catch (CustomerException $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute(
            'admin_manage_bo_tabs_list',
            ['parentId' => (int)\Configuration::get('MTB_ID_PARENT')]
        );
    }

    /**
     * Disable tabs in bulk action.
     *
     * @AdminSecurity(
     *     "is_granted('update', request.get('_legacy_controller'))",
     *     redirectRoute="admin_customers_index",
     *     message="You do not have permission to edit this."
     * )
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function disableBulkAction(Request $request)
    {
        $tabIds = array_map(function ($tabId) {
            return (int) $tabId;
        }, $request->request->get('manage_bo_tabs_managebotabs_bulk', []));

        try {
            $command = new BulkDisableTabCommand($tabIds);

            $this->getCommandBus()->handle($command);

            $this->addFlash('success', $this->trans('Successful update.', 'Admin.Notifications.Success'));
        } catch (TabException $e) {
            $this->addFlash('error', $e->getMessage());
        }

        return $this->redirectToRoute(
            'admin_manage_bo_tabs_list',
            ['parentId' => (int)\Configuration::get('MTB_ID_PARENT')]
        );
    }
}
