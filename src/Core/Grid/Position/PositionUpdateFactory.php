<?php
/**
 * 2007-2021 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

namespace PrestaShop\Module\ManageBoTabs\Core\Grid\Position;

use PrestaShop\PrestaShop\Core\Grid\Position\Exception\PositionDataException;
use PrestaShop\PrestaShop\Core\Grid\Position\PositionDefinition;
use PrestaShop\PrestaShop\Core\Grid\Position\PositionModification;
use PrestaShop\PrestaShop\Core\Grid\Position\PositionModificationCollection;
use PrestaShop\PrestaShop\Core\Grid\Position\PositionUpdate;
use PrestaShop\PrestaShop\Core\Grid\Position\PositionUpdateFactoryInterface;

final class PositionUpdateFactory implements PositionUpdateFactoryInterface
{
    const POSITION_KEY = 'Invalid position %i data, missing %s field.';

    /**
     * @var string
     */
    private $positionsField;

    /**
     * @var string
     */
    private $rowIdField;

    /**
     * @var string
     */
    private $oldPositionField;

    /**
     * @var string
     */
    private $newPositionField;

    /**
     * @var string
     */
    private $parentIdField;

    /**
     * @param string $positionsField
     * @param string $rowIdField
     * @param string $oldPositionField
     * @param string $newPositionField
     * @param string $parentIdField
     */
    public function __construct(
        $positionsField,
        $rowIdField,
        $oldPositionField,
        $newPositionField,
        $parentIdField
    ) {
        $this->positionsField = $positionsField;
        $this->rowIdField = $rowIdField;
        $this->oldPositionField = $oldPositionField;
        $this->newPositionField = $newPositionField;
        $this->parentIdField = $parentIdField;
    }

    /**
     * {@inheritdoc}
     */
    public function buildPositionUpdate(array $data, PositionDefinition $positionDefinition)
    {
        $this->validateData($data);

        $updates = new PositionModificationCollection();
        foreach ($data[$this->positionsField] as $index => $position) {
            $this->validatePositionData($position, $index);

            $updates->add(new PositionModification(
                $position[$this->rowIdField],
                $position[$this->oldPositionField],
                $position[$this->newPositionField]
            ));
        }

        return new PositionUpdate(
            $updates,
            $positionDefinition,
            isset($data[$this->parentIdField]) ? $data[$this->parentIdField] : null
        );
    }

    /**
     * @param array $data
     *
     * @throws PositionDataException
     */
    private function validateData(array $data)
    {
        if (empty($data[$this->positionsField])) {
            throw new PositionDataException(
                'Missing ' . $this->positionsField . ' in your data.',
                'Admin.Notifications.Failure'
            );
        }
    }

    /**
     * Validate the position format, throw a PositionDataException if is not correct.
     *
     * @param array $position
     * @param int $index
     *
     * @throws PositionDataException
     */
    private function validatePositionData(array $position, $index)
    {
        if (!isset($position[$this->rowIdField])) {
            throw new PositionDataException(
                self::POSITION_KEY,
                'Admin.Notifications.Failure',
                [$index, $this->rowIdField]
            );
        }
        if (!isset($position[$this->oldPositionField])) {
            throw new PositionDataException(
                self::POSITION_KEY,
                'Admin.Notifications.Failure',
                [$index, $this->oldPositionField]
            );
        }
        if (!isset($position[$this->newPositionField])) {
            throw new PositionDataException(
                self::POSITION_KEY,
                'Admin.Notifications.Failure',
                [$index, $this->newPositionField]
            );
        }
    }
}
