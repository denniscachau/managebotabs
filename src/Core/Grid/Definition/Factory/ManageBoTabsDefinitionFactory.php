<?php
/**
 * 2007-2018 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2018 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

namespace PrestaShop\Module\ManageBoTabs\Core\Grid\Definition\Factory;

use PrestaShop\Module\ManageBoTabs\Core\Grid\Action\Row\AccessibilityChecker\HasChildAccessibilityChecker;
use PrestaShop\PrestaShop\Core\Grid\Action\Bulk\BulkActionCollection;
use PrestaShop\PrestaShop\Core\Grid\Action\Bulk\Type\SubmitBulkAction;
use PrestaShop\PrestaShop\Core\Grid\Action\Row\RowActionCollection;
use PrestaShop\PrestaShop\Core\Grid\Action\Row\Type\LinkRowAction;
use PrestaShop\PrestaShop\Core\Grid\Column\ColumnCollection;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\ActionColumn;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\BulkActionColumn;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\PositionColumn;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\Common\ToggleColumn;
use PrestaShop\PrestaShop\Core\Grid\Column\Type\DataColumn;
use PrestaShop\PrestaShop\Core\Grid\Definition\Factory\AbstractGridDefinitionFactory;
use PrestaShop\PrestaShop\Core\Hook\HookDispatcherInterface;

/**
 * Class ManageBoTabsDefinitionFactory.
 */
final class ManageBoTabsDefinitionFactory extends AbstractGridDefinitionFactory
{
    /**
     * @var HasChildAccessibilityChecker
     */
    private $hasChildAccessibilityChecker;

    /**
     * @param HookDispatcherInterface $hookDispatcher
     * @param HasChildAccessibilityChecker $hasChildAccessibilityChecker
     */
    public function __construct(
        HookDispatcherInterface $hookDispatcher,
        HasChildAccessibilityChecker $hasChildAccessibilityChecker
    ) {
        parent::__construct($hookDispatcher);

        $this->hasChildAccessibilityChecker = $hasChildAccessibilityChecker;
    }
    /**
     * {@inheritdoc}
     */
    protected function getId()
    {
        return 'manage_bo_tabs';
    }

    /**
     * {@inheritdoc}
     */
    protected function getName()
    {
        return $this->trans('Manage admin menus', [], 'Modules.Managebotabs.Admin');
    }

    /**
     * {@inheritdoc}
     */
    protected function getColumns()
    {
        return (new ColumnCollection())
            ->add(
                (new BulkActionColumn('managebotabs_bulk'))
                    ->setOptions([
                        'bulk_field' => 'id_tab',
                    ])
            )
            ->add(
                (new PositionColumn('position'))
                    ->setName($this->trans('Position', [], 'Admin.Global'))
                    ->setOptions([
                        'id_field' => 'id_tab',
                        'position_field' => 'position',
                        'update_route' => 'admin_manage_bo_tabs_update_positions',
                        'update_method' => 'POST'
                    ])
            )
            // TODO Editable
            ->add(
                (new DataColumn('name'))
                    ->setName($this->trans('Name', [], 'Admin.Global'))
                    ->setOptions([
                        'field' => 'name',
                    ])
            )
            ->add(
                (new DataColumn('class_name'))
                    ->setName($this->trans('Class', [], 'Modules.Managebotabs.Admin'))
                    ->setOptions([
                        'field' => 'class_name',
                    ])
            )
            ->add(
                (new DataColumn('module'))
                    ->setName($this->trans('Module', [], 'Admin.Global'))
                    ->setOptions([
                        'field' => 'module',
                    ])
            )
            ->add(
                (new ToggleColumn('active'))
                    ->setName($this->trans('Displayed', [], 'Admin.Global'))
                    ->setOptions([
                        'field' => 'active',
                        'primary_field' => 'id_tab',
                        'route' => 'admin_manage_bo_tabs_toggle_status',
                        'route_param_name' => 'tabId'
                    ])
            )
            ->add(
                (new ActionColumn('actions'))
                    ->setName($this->trans('Actions', [], 'Admin.Global'))
                    ->setOptions([
                        'actions' => (new RowActionCollection())
                            ->add(
                                (new LinkRowAction('view'))
                                    ->setName($this->trans('View', [], 'Admin.Actions'))
                                    ->setIcon('zoom_in')
                                    ->setOptions([
                                        'accessibility_checker' => $this->hasChildAccessibilityChecker,
                                        'route' => 'admin_manage_bo_tabs_list',
                                        'route_param_name' => 'parentId',
                                        'route_param_field' => 'id_tab',
                                    ])
                            )
                            ->add(
                                (new LinkRowAction('edit'))
                                    ->setName($this->trans('Edit', [], 'Admin.Actions'))
                                    ->setIcon('edit')
                                    ->setOptions([
                                        'route' => 'admin_manage_bo_tabs_edit',
                                        'route_param_name' => 'tabId',
                                        'route_param_field' => 'id_tab',
                                    ])
                            )
                    ])
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function getBulkActions()
    {
        return (new BulkActionCollection())
            ->add(
                (new SubmitBulkAction('enable_selection'))
                    ->setName($this->trans('Display selection', [], 'Modules.Managebotabs.Admin'))
                    ->setOptions([
                        'submit_route' => 'admin_manage_bo_tabs_enable_bulk',
                    ])
            )
            ->add(
                (new SubmitBulkAction('disable_selection'))
                    ->setName($this->trans('Do not display selection', [], 'Modules.Managebotabs.Admin'))
                    ->setOptions([
                        'submit_route' => 'admin_manage_bo_tabs_disable_bulk',
                    ])
            );
    }
}
