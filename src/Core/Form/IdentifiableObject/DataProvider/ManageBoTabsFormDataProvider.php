<?php
/**
 * 2007-2021 PrestaShop.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

namespace PrestaShop\Module\ManageBoTabs\Core\Form\IdentifiableObject\DataProvider;

use PrestaShop\PrestaShop\Core\Form\IdentifiableObject\DataProvider\FormDataProviderInterface;
use PrestaShop\PrestaShop\Core\Form\UndefinedOptionsException;
use Tab;

final class ManageBoTabsFormDataProvider implements FormDataProviderInterface
{

    public function getData($tabId)
    {
        $tab = new Tab($tabId);

        // check that the element exists in db
        if (empty($tab->id)) {
            throw new PrestaShopObjectNotFoundException('Object not found');
        }

        return [
            'tab_name' => $tab->name,
        ];
    }

    public function getDefaultData()
    {
        return [
            'tab_name' => [],
        ];
    }
}
