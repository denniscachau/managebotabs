/**
 * 2007-2021 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

import Grid from '../../../../admin-dev/themes/new-theme/js/components/grid/grid';
import SortingExtension from '../../../../admin-dev/themes/new-theme/js/components/grid/extension/sorting-extension';
import BulkActionCheckboxExtension from '../../../../admin-dev/themes/new-theme/js/components/grid/extension/bulk-action-checkbox-extension';
import SubmitBulkExtension from '../../../../admin-dev/themes/new-theme/js/components/grid/extension/submit-bulk-action-extension';
import SubmitGridExtension from '../../../../admin-dev/themes/new-theme/js/components/grid/extension/submit-grid-action-extension';
import LinkRowActionExtension from '../../../../admin-dev/themes/new-theme/js/components/grid/extension/link-row-action-extension';
import LinkableItem from '../../../../admin-dev/themes/new-theme/js/components/linkable-item';
import ColumnTogglingExtension from '../../../../admin-dev/themes/new-theme/js/components/grid/extension/column-toggling-extension';
import PositionExtension from "../../../../admin-dev/themes/new-theme/js/components/grid/extension/position-extension";
import TranslatableInput from '../../../../admin-dev/themes/new-theme/js/components/translatable-input';

const $ = window.$;

$(() => {
    const mtbGrid = new Grid('manage_bo_tabs');

    mtbGrid.addExtension(new SortingExtension());
    mtbGrid.addExtension(new BulkActionCheckboxExtension());
    mtbGrid.addExtension(new SubmitBulkExtension());
    mtbGrid.addExtension(new SubmitGridExtension());
    mtbGrid.addExtension(new LinkRowActionExtension());
    mtbGrid.addExtension(new ColumnTogglingExtension());
    mtbGrid.addExtension(new PositionExtension());

    new LinkableItem();
    new TranslatableInput({localeInputSelector: '.js-locale-input'});
});
