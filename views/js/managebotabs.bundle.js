!function(e){var t={};function n(r){if(t[r])return t[r].exports;var i=t[r]={i:r,l:!1,exports:{}};return e[r].call(i.exports,i,i.exports,n),i.l=!0,i.exports}n.m=e,n.c=t,n.d=function(e,t,r){n.o(e,t)||Object.defineProperty(e,t,{enumerable:!0,get:r})},n.r=function(e){"undefined"!=typeof Symbol&&Symbol.toStringTag&&Object.defineProperty(e,Symbol.toStringTag,{value:"Module"}),Object.defineProperty(e,"__esModule",{value:!0})},n.t=function(e,t){if(1&t&&(e=n(e)),8&t)return e;if(4&t&&"object"==typeof e&&e&&e.__esModule)return e;var r=Object.create(null);if(n.r(r),Object.defineProperty(r,"default",{enumerable:!0,value:e}),2&t&&"string"!=typeof e)for(var i in e)n.d(r,i,function(t){return e[t]}.bind(null,i));return r},n.n=function(e){var t=e&&e.__esModule?function(){return e.default}:function(){return e};return n.d(t,"a",t),t},n.o=function(e,t){return Object.prototype.hasOwnProperty.call(e,t)},n.p="",n(n.s=5)}([function(e,t){var n;n=function(){return this}();try{n=n||new Function("return this")()}catch(e){"object"==typeof window&&(n=window)}e.exports=n},function(e,t,n){"use strict";(function(e){
/**
 * 2007-2021 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
const n=e.$;t.a=class{constructor(e){this.selector=".ps-sortable-column",this.columns=n(e).find(this.selector)}attach(){this.columns.on("click",e=>{const t=n(e.delegateTarget);this._sortByColumn(t,this._getToggledSortDirection(t))})}sortBy(e,t){const n=this.columns.is(`[data-sort-col-name="${e}"]`);if(!n)throw new Error(`Cannot sort by "${e}": invalid column`);this._sortByColumn(n,t)}_sortByColumn(e,t){window.location=this._getUrl(e.data("sortColName"),"desc"===t?"desc":"asc",e.data("sortPrefix"))}_getToggledSortDirection(e){return"asc"===e.data("sortDirection")?"desc":"asc"}_getUrl(e,t,n){const r=new URL(window.location.href),i=r.searchParams;return n?(i.set(n+"[orderBy]",e),i.set(n+"[sortOrder]",t)):(i.set("orderBy",e),i.set("sortOrder",t)),r.toString()}}}).call(this,n(0))},function(e,t,n){"use strict";(function(e){n.d(t,"a",(function(){return i}));
/**
 * 2007-2021 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
const r=e.$;class i{extend(e){e.getContainer().find("table.table").find(".ps-togglable-row").on("click",e=>{e.preventDefault(),this._toggleValue(r(e.delegateTarget))})}_toggleValue(e){const t=e.data("toggleUrl");this._submitAsForm(t)}_submitAsForm(e){r("<form>",{action:e,method:"POST"}).appendTo("body").submit()}}}).call(this,n(0))},function(e,t,n){"use strict";var r,i="object"==typeof Reflect?Reflect:null,o=i&&"function"==typeof i.apply?i.apply:function(e,t,n){return Function.prototype.apply.call(e,t,n)};r=i&&"function"==typeof i.ownKeys?i.ownKeys:Object.getOwnPropertySymbols?function(e){return Object.getOwnPropertyNames(e).concat(Object.getOwnPropertySymbols(e))}:function(e){return Object.getOwnPropertyNames(e)};var a=Number.isNaN||function(e){return e!=e};function s(){s.init.call(this)}e.exports=s,e.exports.once=function(e,t){return new Promise((function(n,r){function i(){void 0!==o&&e.removeListener("error",o),n([].slice.call(arguments))}var o;"error"!==t&&(o=function(n){e.removeListener(t,i),r(n)},e.once("error",o)),e.once(t,i)}))},s.EventEmitter=s,s.prototype._events=void 0,s.prototype._eventsCount=0,s.prototype._maxListeners=void 0;var l=10;function c(e){if("function"!=typeof e)throw new TypeError('The "listener" argument must be of type Function. Received type '+typeof e)}function d(e){return void 0===e._maxListeners?s.defaultMaxListeners:e._maxListeners}function u(e,t,n,r){var i,o,a,s;if(c(n),void 0===(o=e._events)?(o=e._events=Object.create(null),e._eventsCount=0):(void 0!==o.newListener&&(e.emit("newListener",t,n.listener?n.listener:n),o=e._events),a=o[t]),void 0===a)a=o[t]=n,++e._eventsCount;else if("function"==typeof a?a=o[t]=r?[n,a]:[a,n]:r?a.unshift(n):a.push(n),(i=d(e))>0&&a.length>i&&!a.warned){a.warned=!0;var l=new Error("Possible EventEmitter memory leak detected. "+a.length+" "+String(t)+" listeners added. Use emitter.setMaxListeners() to increase limit");l.name="MaxListenersExceededWarning",l.emitter=e,l.type=t,l.count=a.length,s=l,console&&console.warn&&console.warn(s)}return e}function h(){if(!this.fired)return this.target.removeListener(this.type,this.wrapFn),this.fired=!0,0===arguments.length?this.listener.call(this.target):this.listener.apply(this.target,arguments)}function f(e,t,n){var r={fired:!1,wrapFn:void 0,target:e,type:t,listener:n},i=h.bind(r);return i.listener=n,r.wrapFn=i,i}function g(e,t,n){var r=e._events;if(void 0===r)return[];var i=r[t];return void 0===i?[]:"function"==typeof i?n?[i.listener||i]:[i]:n?function(e){for(var t=new Array(e.length),n=0;n<t.length;++n)t[n]=e[n].listener||e[n];return t}(i):b(i,i.length)}function p(e){var t=this._events;if(void 0!==t){var n=t[e];if("function"==typeof n)return 1;if(void 0!==n)return n.length}return 0}function b(e,t){for(var n=new Array(t),r=0;r<t;++r)n[r]=e[r];return n}Object.defineProperty(s,"defaultMaxListeners",{enumerable:!0,get:function(){return l},set:function(e){if("number"!=typeof e||e<0||a(e))throw new RangeError('The value of "defaultMaxListeners" is out of range. It must be a non-negative number. Received '+e+".");l=e}}),s.init=function(){void 0!==this._events&&this._events!==Object.getPrototypeOf(this)._events||(this._events=Object.create(null),this._eventsCount=0),this._maxListeners=this._maxListeners||void 0},s.prototype.setMaxListeners=function(e){if("number"!=typeof e||e<0||a(e))throw new RangeError('The value of "n" is out of range. It must be a non-negative number. Received '+e+".");return this._maxListeners=e,this},s.prototype.getMaxListeners=function(){return d(this)},s.prototype.emit=function(e){for(var t=[],n=1;n<arguments.length;n++)t.push(arguments[n]);var r="error"===e,i=this._events;if(void 0!==i)r=r&&void 0===i.error;else if(!r)return!1;if(r){var a;if(t.length>0&&(a=t[0]),a instanceof Error)throw a;var s=new Error("Unhandled error."+(a?" ("+a.message+")":""));throw s.context=a,s}var l=i[e];if(void 0===l)return!1;if("function"==typeof l)o(l,this,t);else{var c=l.length,d=b(l,c);for(n=0;n<c;++n)o(d[n],this,t)}return!0},s.prototype.addListener=function(e,t){return u(this,e,t,!1)},s.prototype.on=s.prototype.addListener,s.prototype.prependListener=function(e,t){return u(this,e,t,!0)},s.prototype.once=function(e,t){return c(t),this.on(e,f(this,e,t)),this},s.prototype.prependOnceListener=function(e,t){return c(t),this.prependListener(e,f(this,e,t)),this},s.prototype.removeListener=function(e,t){var n,r,i,o,a;if(c(t),void 0===(r=this._events))return this;if(void 0===(n=r[e]))return this;if(n===t||n.listener===t)0==--this._eventsCount?this._events=Object.create(null):(delete r[e],r.removeListener&&this.emit("removeListener",e,n.listener||t));else if("function"!=typeof n){for(i=-1,o=n.length-1;o>=0;o--)if(n[o]===t||n[o].listener===t){a=n[o].listener,i=o;break}if(i<0)return this;0===i?n.shift():function(e,t){for(;t+1<e.length;t++)e[t]=e[t+1];e.pop()}(n,i),1===n.length&&(r[e]=n[0]),void 0!==r.removeListener&&this.emit("removeListener",e,a||t)}return this},s.prototype.off=s.prototype.removeListener,s.prototype.removeAllListeners=function(e){var t,n,r;if(void 0===(n=this._events))return this;if(void 0===n.removeListener)return 0===arguments.length?(this._events=Object.create(null),this._eventsCount=0):void 0!==n[e]&&(0==--this._eventsCount?this._events=Object.create(null):delete n[e]),this;if(0===arguments.length){var i,o=Object.keys(n);for(r=0;r<o.length;++r)"removeListener"!==(i=o[r])&&this.removeAllListeners(i);return this.removeAllListeners("removeListener"),this._events=Object.create(null),this._eventsCount=0,this}if("function"==typeof(t=n[e]))this.removeListener(e,t);else if(void 0!==t)for(r=t.length-1;r>=0;r--)this.removeListener(e,t[r]);return this},s.prototype.listeners=function(e){return g(this,e,!0)},s.prototype.rawListeners=function(e){return g(this,e,!1)},s.listenerCount=function(e,t){return"function"==typeof e.listenerCount?e.listenerCount(t):p.call(e,t)},s.prototype.listenerCount=p,s.prototype.eventNames=function(){return this._eventsCount>0?r(this._events):[]}},function(e,t){!function(e,t,n,r){e(n).ready((function(){function t(e){for(var t={},n=e.match(/([^;:]+)/g)||[];n.length;)t[n.shift()]=n.shift().trim();return t}e("table").each((function(){"dnd"===e(this).data("table")&&e(this).tableDnD({onDragStyle:e(this).data("ondragstyle")&&t(e(this).data("ondragstyle"))||null,onDropStyle:e(this).data("ondropstyle")&&t(e(this).data("ondropstyle"))||null,onDragClass:void 0===e(this).data("ondragclass")?"tDnD_whileDrag":e(this).data("ondragclass"),onDrop:e(this).data("ondrop")&&new Function("table","row",e(this).data("ondrop")),onDragStart:e(this).data("ondragstart")&&new Function("table","row",e(this).data("ondragstart")),onDragStop:e(this).data("ondragstop")&&new Function("table","row",e(this).data("ondragstop")),scrollAmount:e(this).data("scrollamount")||5,sensitivity:e(this).data("sensitivity")||10,hierarchyLevel:e(this).data("hierarchylevel")||0,indentArtifact:e(this).data("indentartifact")||'<div class="indent">&nbsp;</div>',autoWidthAdjust:e(this).data("autowidthadjust")||!0,autoCleanRelations:e(this).data("autocleanrelations")||!0,jsonPretifySeparator:e(this).data("jsonpretifyseparator")||"\t",serializeRegexp:e(this).data("serializeregexp")&&new RegExp(e(this).data("serializeregexp"))||/[^\-]*$/,serializeParamName:e(this).data("serializeparamname")||!1,dragHandle:e(this).data("draghandle")||null})}))})),jQuery.tableDnD={currentTable:null,dragObject:null,mouseOffset:null,oldX:0,oldY:0,build:function(t){return this.each((function(){this.tableDnDConfig=e.extend({onDragStyle:null,onDropStyle:null,onDragClass:"tDnD_whileDrag",onDrop:null,onDragStart:null,onDragStop:null,scrollAmount:5,sensitivity:10,hierarchyLevel:0,indentArtifact:'<div class="indent">&nbsp;</div>',autoWidthAdjust:!0,autoCleanRelations:!0,jsonPretifySeparator:"\t",serializeRegexp:/[^\-]*$/,serializeParamName:!1,dragHandle:null},t||{}),e.tableDnD.makeDraggable(this),this.tableDnDConfig.hierarchyLevel&&e.tableDnD.makeIndented(this)})),this},makeIndented:function(t){var n,r,i=t.tableDnDConfig,o=t.rows,a=e(o).first().find("td:first")[0],s=0,l=0;if(e(t).hasClass("indtd"))return null;r=e(t).addClass("indtd").attr("style"),e(t).css({whiteSpace:"nowrap"});for(var c=0;c<o.length;c++)l<e(o[c]).find("td:first").text().length&&(l=e(o[c]).find("td:first").text().length,n=c);for(e(a).css({width:"auto"}),c=0;c<i.hierarchyLevel;c++)e(o[n]).find("td:first").prepend(i.indentArtifact);for(a&&e(a).css({width:a.offsetWidth}),r&&e(t).css(r),c=0;c<i.hierarchyLevel;c++)e(o[n]).find("td:first").children(":first").remove();return i.hierarchyLevel&&e(o).each((function(){(s=e(this).data("level")||0)<=i.hierarchyLevel&&e(this).data("level",s)||e(this).data("level",0);for(var t=0;t<e(this).data("level");t++)e(this).find("td:first").prepend(i.indentArtifact)})),this},makeDraggable:function(t){var n=t.tableDnDConfig;n.dragHandle&&e(n.dragHandle,t).each((function(){e(this).bind("touchstart mousedown",(function(r){return e.tableDnD.initialiseDrag(e(this).parents("tr")[0],t,this,r,n),!1}))}))||e(t.rows).each((function(){e(this).hasClass("nodrag")?e(this).css("cursor",""):e(this).bind("touchstart mousedown",(function(r){if("TD"===r.target.tagName)return e.tableDnD.initialiseDrag(this,t,this,r,n),!1})).css("cursor","move")}))},currentOrder:function(){var t=this.currentTable.rows;return e.map(t,(function(t){return(e(t).data("level")+t.id).replace(/\s/g,"")})).join("")},initialiseDrag:function(t,r,i,o,a){this.dragObject=t,this.currentTable=r,this.mouseOffset=this.getMouseOffset(i,o),this.originalOrder=this.currentOrder(),e(n).bind("touchmove mousemove",this.mousemove).bind("touchend mouseup",this.mouseup),a.onDragStart&&a.onDragStart(r,i)},updateTables:function(){this.each((function(){this.tableDnDConfig&&e.tableDnD.makeDraggable(this)}))},mouseCoords:function(e){return e.originalEvent.changedTouches?{x:e.originalEvent.changedTouches[0].clientX,y:e.originalEvent.changedTouches[0].clientY}:e.pageX||e.pageY?{x:e.pageX,y:e.pageY}:{x:e.clientX+n.body.scrollLeft-n.body.clientLeft,y:e.clientY+n.body.scrollTop-n.body.clientTop}},getMouseOffset:function(e,n){var r,i;return n=n||t.event,i=this.getPosition(e),{x:(r=this.mouseCoords(n)).x-i.x,y:r.y-i.y}},getPosition:function(e){var t=0,n=0;for(0===e.offsetHeight&&(e=e.firstChild);e.offsetParent;)t+=e.offsetLeft,n+=e.offsetTop,e=e.offsetParent;return{x:t+=e.offsetLeft,y:n+=e.offsetTop}},autoScroll:function(e){var r=this.currentTable.tableDnDConfig,i=t.pageYOffset,o=t.innerHeight?t.innerHeight:n.documentElement.clientHeight?n.documentElement.clientHeight:n.body.clientHeight;n.all&&(void 0!==n.compatMode&&"BackCompat"!==n.compatMode?i=n.documentElement.scrollTop:void 0!==n.body&&(i=n.body.scrollTop)),e.y-i<r.scrollAmount&&t.scrollBy(0,-r.scrollAmount)||o-(e.y-i)<r.scrollAmount&&t.scrollBy(0,r.scrollAmount)},moveVerticle:function(e,t){0!==e.vertical&&t&&this.dragObject!==t&&this.dragObject.parentNode===t.parentNode&&(0>e.vertical&&this.dragObject.parentNode.insertBefore(this.dragObject,t.nextSibling)||0<e.vertical&&this.dragObject.parentNode.insertBefore(this.dragObject,t))},moveHorizontal:function(t,n){var r,i=this.currentTable.tableDnDConfig;if(!i.hierarchyLevel||0===t.horizontal||!n||this.dragObject!==n)return null;r=e(n).data("level"),0<t.horizontal&&r>0&&e(n).find("td:first").children(":first").remove()&&e(n).data("level",--r),0>t.horizontal&&r<i.hierarchyLevel&&e(n).prev().data("level")>=r&&e(n).children(":first").prepend(i.indentArtifact)&&e(n).data("level",++r)},mousemove:function(t){var n,r,i,o,a,s=e(e.tableDnD.dragObject),l=e.tableDnD.currentTable.tableDnDConfig;return t&&t.preventDefault(),!!e.tableDnD.dragObject&&("touchmove"===t.type&&event.preventDefault(),l.onDragClass&&s.addClass(l.onDragClass)||s.css(l.onDragStyle),o=(r=e.tableDnD.mouseCoords(t)).x-e.tableDnD.mouseOffset.x,a=r.y-e.tableDnD.mouseOffset.y,e.tableDnD.autoScroll(r),n=e.tableDnD.findDropTargetRow(s,a),i=e.tableDnD.findDragDirection(o,a),e.tableDnD.moveVerticle(i,n),e.tableDnD.moveHorizontal(i,n),!1)},findDragDirection:function(e,t){var n=this.currentTable.tableDnDConfig.sensitivity,r=this.oldX,i=this.oldY,o={horizontal:e>=r-n&&e<=r+n?0:e>r?-1:1,vertical:t>=i-n&&t<=i+n?0:t>i?-1:1};return 0!==o.horizontal&&(this.oldX=e),0!==o.vertical&&(this.oldY=t),o},findDropTargetRow:function(t,n){for(var r=0,i=this.currentTable.rows,o=this.currentTable.tableDnDConfig,a=0,s=null,l=0;l<i.length;l++)if(s=i[l],a=this.getPosition(s).y,r=parseInt(s.offsetHeight)/2,0===s.offsetHeight&&(a=this.getPosition(s.firstChild).y,r=parseInt(s.firstChild.offsetHeight)/2),n>a-r&&n<a+r)return t.is(s)||o.onAllowDrop&&!o.onAllowDrop(t,s)||e(s).hasClass("nodrop")?null:s;return null},processMouseup:function(){if(!this.currentTable||!this.dragObject)return null;var t=this.currentTable.tableDnDConfig,r=this.dragObject,i=0,o=0;e(n).unbind("touchmove mousemove",this.mousemove).unbind("touchend mouseup",this.mouseup),t.hierarchyLevel&&t.autoCleanRelations&&e(this.currentTable.rows).first().find("td:first").children().each((function(){(o=e(this).parents("tr:first").data("level"))&&e(this).parents("tr:first").data("level",--o)&&e(this).remove()}))&&t.hierarchyLevel>1&&e(this.currentTable.rows).each((function(){if((o=e(this).data("level"))>1)for(i=e(this).prev().data("level");o>i+1;)e(this).find("td:first").children(":first").remove(),e(this).data("level",--o)})),t.onDragClass&&e(r).removeClass(t.onDragClass)||e(r).css(t.onDropStyle),this.dragObject=null,t.onDrop&&this.originalOrder!==this.currentOrder()&&e(r).hide().fadeIn("fast")&&t.onDrop(this.currentTable,r),t.onDragStop&&t.onDragStop(this.currentTable,r),this.currentTable=null},mouseup:function(t){return t&&t.preventDefault(),e.tableDnD.processMouseup(),!1},jsonize:function(e){var t=this.currentTable;return e?JSON.stringify(this.tableData(t),null,t.tableDnDConfig.jsonPretifySeparator):JSON.stringify(this.tableData(t))},serialize:function(){return e.param(this.tableData(this.currentTable))},serializeTable:function(e){for(var t="",n=e.tableDnDConfig.serializeParamName||e.id,r=e.rows,i=0;i<r.length;i++){t.length>0&&(t+="&");var o=r[i].id;o&&e.tableDnDConfig&&e.tableDnDConfig.serializeRegexp&&(t+=n+"[]="+(o=o.match(e.tableDnDConfig.serializeRegexp)[0]))}return t},serializeTables:function(){var t=[];return e("table").each((function(){this.id&&t.push(e.param(e.tableDnD.tableData(this)))})),t.join("&")},tableData:function(t){var n,r,i,o,a=t.tableDnDConfig,s=[],l=0,c=0,d=null,u={};if(t||(t=this.currentTable),!t||!t.rows||!t.rows.length)return{error:{code:500,message:"Not a valid table."}};if(!t.id&&!a.serializeParamName)return{error:{code:500,message:"No serializable unique id provided."}};o=a.autoCleanRelations&&t.rows||e.makeArray(t.rows),n=function(e){return e&&a&&a.serializeRegexp?e.match(a.serializeRegexp)[0]:e},u[i=r=a.serializeParamName||t.id]=[],!a.autoCleanRelations&&e(o[0]).data("level")&&o.unshift({id:"undefined"});for(var h=0;h<o.length;h++)if(a.hierarchyLevel){if(0===(c=e(o[h]).data("level")||0))i=r,s=[];else if(c>l)s.push([i,l]),i=n(o[h-1].id);else if(c<l)for(var f=0;f<s.length;f++)s[f][1]===c&&(i=s[f][0]),s[f][1]>=l&&(s[f][1]=0);l=c,e.isArray(u[i])||(u[i]=[]),(d=n(o[h].id))&&u[i].push(d)}else(d=n(o[h].id))&&u[i].push(d);return u}},jQuery.fn.extend({tableDnD:e.tableDnD.build,tableDnDUpdate:e.tableDnD.updateTables,tableDnDSerialize:e.proxy(e.tableDnD.serialize,e.tableDnD),tableDnDSerializeAll:e.tableDnD.serializeTables,tableDnDData:e.proxy(e.tableDnD.tableData,e.tableDnD)})}(jQuery,window,window.document)},function(e,t,n){"use strict";n.r(t);
/**
 * 2007-2021 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
const r=window.$;class i{constructor(e){this.id=e,this.$container=r("#"+this.id+"_grid")}getId(){return this.id}getContainer(){return this.$container}getHeaderContainer(){return this.$container.closest(".js-grid-panel").find(".js-grid-header")}addExtension(e){e.extend(this)}}var o=n(1);
/**
 * 2007-2021 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */class a{extend(e){const t=e.getContainer().find("table.table");new o.a(t).attach()}}
/**
 * 2007-2021 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */const s=window.$;class l{extend(e){this._handleBulkActionCheckboxSelect(e),this._handleBulkActionSelectAllCheckbox(e)}_handleBulkActionSelectAllCheckbox(e){e.getContainer().on("change",".js-bulk-action-select-all",t=>{const n=s(t.currentTarget).is(":checked");n?this._enableBulkActionsBtn(e):this._disableBulkActionsBtn(e),e.getContainer().find(".js-bulk-action-checkbox").prop("checked",n)})}_handleBulkActionCheckboxSelect(e){e.getContainer().on("change",".js-bulk-action-checkbox",()=>{e.getContainer().find(".js-bulk-action-checkbox:checked").length>0?this._enableBulkActionsBtn(e):this._disableBulkActionsBtn(e)})}_enableBulkActionsBtn(e){e.getContainer().find(".js-bulk-actions-btn").prop("disabled",!1)}_disableBulkActionsBtn(e){e.getContainer().find(".js-bulk-actions-btn").prop("disabled",!0)}}
/**
 * 2007-2021 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */const c=window.$;class d{constructor(){return{extend:e=>this.extend(e)}}extend(e){e.getContainer().on("click",".js-bulk-action-submit-btn",t=>{this.submit(t,e)})}submit(e,t){const n=c(e.currentTarget),r=n.data("confirm-message");if(void 0!==r&&0<r.length&&!confirm(r))return;const i=c("#"+t.getId()+"_filter_form");i.attr("action",n.data("form-url")),i.attr("method",n.data("form-method")),i.submit()}}
/**
 * 2007-2021 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */const u=window.$;class h{constructor(){return{extend:e=>this.extend(e)}}extend(e){e.getHeaderContainer().on("click",".js-grid-action-submit-btn",t=>{this.handleSubmit(t,e)})}handleSubmit(e,t){const n=u(e.currentTarget),r=n.data("confirm-message");if(void 0!==r&&0<r.length&&!confirm(r))return;const i=u("#"+t.getId()+"_filter_form");i.attr("action",n.data("url")),i.attr("method",n.data("method")),i.find('input[name="'+t.getId()+'[_token]"]').val(n.data("csrf")),i.submit()}}
/**
 * 2007-2021 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */const f=window.$;class g{extend(e){e.getContainer().on("click",".js-link-row-action",e=>{const t=f(e.currentTarget).data("confirm-message");t.length&&!confirm(t)&&e.preventDefault()})}}
/**
 * 2007-2021 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */const p=window.$;class b{constructor(){p(document).on("click",".js-linkable-item",e=>{window.location=p(e.currentTarget).data("linkable-href")})}}var v=n(2);n(4);
/**
 * 2007-2021 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
const m=window.$;class D{constructor(){return{extend:e=>this.extend(e)}}extend(e){this.grid=e,this._addIdsToGridTableRows(),e.getContainer().find(".js-grid-table").tableDnD({onDragClass:"position-row-while-drag",dragHandle:".js-drag-handle",onDrop:(e,t)=>this._handlePositionChange(t)}),e.getContainer().find(".js-drag-handle").hover((function(){m(this).closest("tr").addClass("hover")}),(function(){m(this).closest("tr").removeClass("hover")}))}_handlePositionChange(e){const t=m(e).find(".js-"+this.grid.getId()+"-position:first"),n=t.data("update-url"),r=t.data("update-method"),i=parseInt(t.data("pagination-offset"),10),o={positions:this._getRowsPositions(i)};this._updatePosition(n,o,r)}_getRowsPositions(e){const t=JSON.parse(m.tableDnD.jsonize())[this.grid.getId()+"_grid_table"],n=/^row_(\d+)_(\d+)$/,r=t.length,i=[];let o,a;for(a=0;a<r;++a)o=n.exec(t[a]),i.push({rowId:o[1],newPosition:e+a,oldPosition:parseInt(o[2],10)});return i}_addIdsToGridTableRows(){this.grid.getContainer().find(".js-grid-table .js-"+this.grid.getId()+"-position").each((e,t)=>{const n=m(t),r=`row_${n.data("id")}_${n.data("position")}`;n.closest("tr").attr("id",r),n.closest("td").addClass("js-drag-handle")})}_updatePosition(e,t,n){const r=["GET","POST"].includes(n),i=m("<form>",{action:e,method:r?n:"POST"}).appendTo("body"),o=t.positions.length;let a;for(let e=0;e<o;++e)a=t.positions[e],i.append(m("<input>",{type:"hidden",name:"positions["+e+"][rowId]",value:a.rowId}),m("<input>",{type:"hidden",name:"positions["+e+"][oldPosition]",value:a.oldPosition}),m("<input>",{type:"hidden",name:"positions["+e+"][newPosition]",value:a.newPosition}));r||i.append(m("<input>",{type:"hidden",name:"_method",value:n})),i.submit()}}var y=n(3);
/**
 * 2007-2021 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */
const w=new(n.n(y).a),_=window.$;
/**
 * 2007-2021 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */var C=class{constructor(e){e=e||{},this.localeItemSelector=e.localeItemSelector||".js-locale-item",this.localeButtonSelector=e.localeButtonSelector||".js-locale-btn",this.localeInputSelector=e.localeInputSelector||".js-locale-input",_("body").on("click",this.localeItemSelector,this.toggleLanguage.bind(this)),w.on("languageSelected",this.toggleInputs.bind(this))}toggleLanguage(e){const t=_(e.target),n=t.closest("form");w.emit("languageSelected",{selectedLocale:t.data("locale"),form:n})}toggleInputs(e){const t=e.form,n=e.selectedLocale,r=t.find(this.localeButtonSelector),i=r.data("change-language-url");r.text(n),t.find(this.localeInputSelector).addClass("d-none"),t.find(`${this.localeInputSelector}.js-locale-${n}`).removeClass("d-none"),i&&this._saveSelectedLanguage(i,n)}_saveSelectedLanguage(e,t){_.post({url:e,data:{language_iso_code:t}})}};
/**
 * 2007-2021 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2021 PrestaShop SA
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */(0,window.$)(()=>{const e=new i("manage_bo_tabs");e.addExtension(new a),e.addExtension(new l),e.addExtension(new d),e.addExtension(new h),e.addExtension(new g),e.addExtension(new v.a),e.addExtension(new D),new b,new C({localeInputSelector:".js-locale-input"})})}]);